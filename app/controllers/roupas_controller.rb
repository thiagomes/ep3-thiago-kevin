class RoupasController < ApplicationController
  before_action :set_roupa, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /roupas
  # GET /roupas.json
  def index
    @roupas = Roupa.all
  end

  # GET /roupas/1
  # GET /roupas/1.json
  def show
  end

  # GET /roupas/new
  def new
    @roupa = Roupa.new
  end

  # GET /roupas/1/edit
  def edit
  end

  # POST /roupas
  # POST /roupas.json
  def create
    @roupa = Roupa.new(roupa_params)

    respond_to do |format|
      if @roupa.save
        format.html { redirect_to @roupa, notice: 'Roupa was successfully created.' }
        format.json { render :show, status: :created, location: @roupa }
      else
        format.html { render :new }
        format.json { render json: @roupa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roupas/1
  # PATCH/PUT /roupas/1.json
  def update
    respond_to do |format|
      if @roupa.update(roupa_params)
        format.html { redirect_to @roupa, notice: 'Roupa was successfully updated.' }
        format.json { render :show, status: :ok, location: @roupa }
      else
        format.html { render :edit }
        format.json { render json: @roupa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roupas/1
  # DELETE /roupas/1.json
  def destroy
    @roupa.destroy
    respond_to do |format|
      format.html { redirect_to roupas_url, notice: 'Roupa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_roupa
      @roupa = Roupa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def roupa_params
      params.require(:roupa).permit(:modelo, :preco, :picture)
    end
end
