class CreateRoupas < ActiveRecord::Migration[5.2]
  def change
    create_table :roupas do |t|
      t.string :modelo
      t.string :preco
      t.string :picture

      t.timestamps
    end
  end
end
