require "application_system_test_case"

class RoupasTest < ApplicationSystemTestCase
  setup do
    @roupa = roupas(:one)
  end

  test "visiting the index" do
    visit roupas_url
    assert_selector "h1", text: "Roupas"
  end

  test "creating a Roupa" do
    visit roupas_url
    click_on "New Roupa"

    fill_in "Modelo", with: @roupa.modelo
    fill_in "Picture", with: @roupa.picture
    fill_in "Preco", with: @roupa.preco
    click_on "Create Roupa"

    assert_text "Roupa was successfully created"
    click_on "Back"
  end

  test "updating a Roupa" do
    visit roupas_url
    click_on "Edit", match: :first

    fill_in "Modelo", with: @roupa.modelo
    fill_in "Picture", with: @roupa.picture
    fill_in "Preco", with: @roupa.preco
    click_on "Update Roupa"

    assert_text "Roupa was successfully updated"
    click_on "Back"
  end

  test "destroying a Roupa" do
    visit roupas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Roupa was successfully destroyed"
  end
end
